### Ingredients

9.5 lbs Pale Liquid Extract (8.0 SRM) Extract 90.83 % 
1 lbs Caramel/Crystal Malt 40L (40.0 SRM) Grain 9.17 % 

1.25 oz Warrior [15.00 %] (60 min) Hops 26.7 IBU 
1.00 oz Amarillo Gold [8.50 %] (35 min) Hops 10.0 IBU 
1.00 oz Simcoe [12.00 %] (30 min) Hops 13.1 IBU 

1.00 oz Amarillo Gold [8.50 %] (Dry Hop 7 days) Hops
0.50 oz Simcoe [12.00 %] (Dry Hop 7 days) Hops 

### Steps

Steep the crystal at 150-155 degrees for 20 minutes.

Remove the grains, and discard them.

Bring to a boil. When it is boiling, remove it from the heat (take it OFF the burner!) And stir in the LME. Put it back on the burner, and bring it to a boil.

When it is boiling, add approximately half of the warrior hops you have.

Timer for 60 minutes.

After that, continue adding a pellet or two or five, every minute or so, until there is 35 minutes left on your timer.  Try to time it so you are out of warrior pellets at 35 minutes left.

Mix the rest of the hops together in a bowl. When you have 35 minutes left, start adding those hops just a few at a time, trying to end with the last of them when your timer hits 0 and you turn the flame off.

Chill the wort rapidly, add to carboy.

Check the temperature to ensure it is around 70 degrees, and pitch a neutral yeast like American ale yeast (1056), or dry yeast like nottingham or safale s05.

Ferment until completely done, and then allow to rest. Two weeks is pretty good.

Then, rack to a clean 5 gallon carboy and dry hop with the dryhopping hops. You can either just put them into the fermenter and rack onto them, or use a hops bag if you=d like. Just don=t pack them tightly, you want the beer to be in contact with the hops. Use two or three bags if you need them. After about a week, you can rack to a bottling bucket avoiding the hops chunks, and bottle. Prime as usual, with approx. 3.5- 4 ounces priming sugar for 5 gallons of beer. You may have less than 5 gallons, due to the hops sucking up some of the beer, that=s why 3.5-4 ounces is a good bet.
