### Ingredients

7.5 lbs. Hollander Dutch dark extract
1 lb. chocolate malt
1 lb. roasted barley
0.75 lb. black malt
0.5 lb. British crystal malt, 40° Lovibond
0.5 lb. dextrin malt
2 oz. Eroica hops (12.6% alpha acid), for 60 min.
1 oz. Kent Goldings hops (5% alpha acid), for 1 min.
6 tsp. gypsum
1 tsp. Irish moss
Wyeast 1084 (Irish ale yeast)

### Steps

MEDEARIS MAD STOUT
"A sweet stout with a strong flavor and high alcohol content."
5 gallons, extract/specialty grains

Grain must be steeped separately at 150° to 160° F for 25 to 30 minutes in 2 gals. water. Add extract and Eroica hops and boil for 60 minutes. Add Kent Goldings for last minute of boil. Remove from heat, cover, and let stand for 10 minutes before cooling the wort. Top off to 5 gals. and pitch yeast. Ferment and prime with corn sugar.

