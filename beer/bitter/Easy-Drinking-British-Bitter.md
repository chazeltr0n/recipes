### Ingredients

4 lbs.   Alexander's extra light malt extract syrup
1 lb.    Laaglander dry malt extract
0.5 lb.  Belgian biscuit malt
0.5 lb.  British crystal malt
2 oz.    Flaked barley
2 oz.    Target hops (10% alpha)
1 oz.    Kent Golding or Fuggle
1/2 cup  priming sugar

### Steps

Crush malts, soak with flaked barley in 1 gal. water at 150° F for one hour.

Sparge grains with hot (170° F) water, collecting a total of about 2.5 to 3 gals. for boil.

Boil for 15 minutes, then add Target hops.

At end of 90 minute boil, add the finishing hops.

Strain wort into 2.5 gals. pre- boiled, chilled wort (to make 5 gals.) Aerate thoroughly!

Pitch one packet of Edme dry ale yeast.

When krausen falls, rack to closed secondary fermenter and follow normal bottling/kegging procedures, but reduce priming sugar to about 1/2 cup.
  (Experiment with various sugars at this stage: invert sugar or dark brown sugar are interesting possibilities).

OG = 1.035

FG = 1.006


### Resources

https://byo.com/stories/item/333-british-bitter-style-of-the-month

