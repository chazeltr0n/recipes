### Bolar Roast

##### Steps

* 1 large bolar roast
* 1 1/2 cups fo red wine (used some random zinfandel)
* 3 tablespoons of coconut oil
* assorted chopped vegetables (used zucchini, potatoes, carrots)
* 1 large onion, quartered
* 2 cloves of garlic
* 2 tablespoons of pepper
* 1 teaspoon of salt
* 1 bay leaf

##### Instructions

Low for 10-12 hours!

