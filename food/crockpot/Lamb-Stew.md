### Lamb Stew

##### Steps

* about 2 lbs of lamb, cubed
* 1/4 cup all purpose flour
* 1 teaspoon of salt
* 1/2 teaspoon of ground black pepper
* 2 cloves of garlic, minced
* 1 bay leaf
* 1 teaspoon of paprika
* 1 onion, coarsley chopped
* 1 1/2 cups of beef or vegetable broth
* 2 large sweet potatoes, diced
* 4 large carrots, sliced
* 1/2 cup red wine (used Prospect & Main cab sauv)

##### Instructions

Place meat in the slow cooker, cover with flour, salt, pepper.

Mix everything else in.

Low for 10-12 hours!
